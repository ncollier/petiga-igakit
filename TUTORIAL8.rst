.. role:: option(literal)
.. role:: file(literal)
.. _TUTORIAL8:

Nonlinear Elasticity and Mixed Language Programming (*)
=======================================================

**Objective:** At the end of this tutorial you will see how functionality can be written in Fortran and then called from the C-based PetIGA library. 

**Assumptions:** 


Writing Tensor Operations in Fortran
------------------------------------


Calling Fortran from C
----------------------


Dissecting :file:`HyperElasticity.c`
------------------------------------


Writing Residuals and Jacobians in Fortran
------------------------------------------


