.. role:: envvar(literal)
.. role:: command(literal)
.. role:: file(literal)
.. role:: ref(title-reference)
.. _OVERVIEW:

Overview
========

PETSc provides a rich environment for modeling scientific applications
as well as for rapid algorithm design and prototyping. The library
enable easy customization and extension of both algorithms and
implementations. This approach promotes code reuse and
flexibility. PETSc is designed in an object-oriented style (in C also
with Fortran interface), with components that may be changed via a
command-line interface at runtime. A few of these objects and their
types include:

* Matrices (Mat) and Vectors (Vec)
* Krylov subspace methods (KSP) and preconditioners (PC) 
* Nonlinear solvers (SNES) 
* Time stepping algorithms (TS)

PetIGA can be thought of as a thin layer on top of PETSc which allows
for parallel assembly of matrices and vectors coming from discretized
integral equations. PetIGA is designed in the same style of PETSc,
adding its own collection of objects. The commonly accessed ones are:

* The context containing all the discretization information (IGA)
* An axis in one parametric direction of the B-spline space (IGAAxis)
* A edge/face of the B-spline space (IGABoundary)
* A quadrature/collocation point (IGAPoint)

These tutorials are not meant to be exhaustive, but rather
demonstrative of the underlying philosophy and intended usage of these
libraries. Obviously we do not speak for the PETSc developers, yet try
to carry much of the insight which has made this project successful to
PetIGA and therefore the practitioner. We write them with a particular
reader in mind: one who works in computation (perhaps limited to
Matlab) but with limited experience in using libraries. Having once
been such a researcher, we know the challenge it is to dive into to
learning and using libraries. We hope that such a collection of
tutorials is useful in bringing more researchers to utilize the
excellents tools developed and freely offered by a great number of
experts.

.. Local Variables:
.. mode: rst
.. End:
