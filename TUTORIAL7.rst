.. role:: option(literal)
.. role:: file(literal)
.. _TUTORIAL7:

Time-dependent, non-linear problems with Cahn-Hilliard (*)
==========================================================

**Objective:** 

**Assumptions:** 

Difference from Linear Problems
-------------------------------

Explain SetUserFunction, SetUserIFunction, SetUserIEFunction

Dissecting the Residual
-----------------------

Verifying the Jacobian
----------------------

Monitor Functions
-----------------

Free energy, output
